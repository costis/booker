require 'spec_helper'

module Booker
  describe Customer do

    before(:each) do
      customers_filepath = File.expand_path("../../fixtures/travellers.json", __FILE__)
      json_file = File.read customers_filepath
      @customers = JSON.parse(json_file, { :object_class => Booker::Customer })
    end

    describe ".set_data" do
      it "should set the collection" do
        Customer.set_data @customers
        Customer.all.should_not be_empty
      end
    end

    describe ".find_by_id" do
      it "should locate a Customer object by id" do
        res = Customer.find_by_id(1)
        res.should_not be_nil
        res.name.should eq "Customer 1"
      end
    end
  end
end
