require 'spec_helper'

module Booker
  describe Booking do

    before(:each) do
      travellers_filepath = File.expand_path("../../fixtures/travellers.json", __FILE__)
      accommodations_filepath = File.expand_path("../../fixtures/accommodation.json", __FILE__)

      customers_file = File.read travellers_filepath
      @customers = JSON.parse(customers_file, { :object_class => Booker::Customer })

      accommodations_file = File.read accommodations_filepath
      @accommodations = JSON.parse(accommodations_file, { :object_class => Booker::Accommodation })

      @customer = @customers.find { |c| c.id == 1}
      @customer_2 = @customers.find { |c| c.id == 2}
      @accommodation = @accommodations.find { |c| c.id == 1}
      @accommodation_2 = @accommodations.find { |c| c.id == 2}
    end

    after(:each) do
      Booking.all.clear
    end

    describe "#initialize" do
      it "should create a new Booking with correct values" do
        booking = Booking.new(@customer, @accommodation)
        booking.customer.should eq @customer
        booking.accommodation.should eq @accommodation
      end
    end

    describe ".create_booking" do
      it "should add new Booking object to collection " do
        Booking.create_booking(@customer, @accommodation)
        Booking.all.should_not be_empty
        Booking.all.size.should eq 1
      end

      it "should return a Booking object with correct values " do
        booking = Booking.create_booking(@customer, @accommodation)
        booking.customer.should eq @customer
        booking.accommodation.should eq @accommodation
      end

      it "should decrease free rooms for booked Accommodation" do
          accommodation = mock(Accommodation)
          accommodation.stub(:reduce_free_capacity)
          accommodation.should_receive(:reduce_free_capacity)
          Booking.create_booking(@customer, accommodation)
      end
    end

    describe ".find_bookings_by_accommodation_id" do
      it "should return bookings by accommodation" do
        booking_1 = Booking.create_booking(@customer, @accommodation)
        booking_2 = Booking.create_booking(@customer_2, @accommodation)
        bookings  = Booking.find_by_accommodation_id(@accommodation.id)

        bookings.size.should eq 2
        bookings.should include(booking_1)
        bookings.should include(booking_2)
      end
    end

    describe ".find_by_customer_id" do
      it "should return bookings by customer" do
        booking_1 = Booking.create_booking(@customer, @accommodation)
        booking_2 = Booking.create_booking(@customer, @accommodation_2)
        bookings  = Booking.find_by_customer_id(@customer.id)

        bookings.size.should eq 2
        bookings.should include(booking_1)
        bookings.should include(booking_2)
      end
    end

  end
end
