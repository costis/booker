require 'spec_helper'

describe Booker::HashExtension do
  before(:each) do
    Hash.send(:include, Booker::HashExtension)
    @hash_obj = Hash.new
  end

  it "should respond to attribute reads" do
    @hash_obj["attribute_one"] = "attribute_one value"
    @hash_obj.attribute_one.should eq "attribute_one value"
  end

  it "should respond to attribute writes" do
    @hash_obj.attribute_two = "attribute_two value"
    @hash_obj["attribute_two"].should eq "attribute_two value"
  end

  it "should raise exception for non existant keys" do
    lambda {@hash_obj.a_non_existant_attribute}.should raise_error(NoMethodError)
  end

end
