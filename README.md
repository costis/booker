Project Description
=========================================

We are trying to find accommodation matches for people who want to go on holiday. There are two sets of data: travellers.json and accommodation.json

Travellers
----------
`data/travellers.json` contains people, their desired price range for accommodation, and their requirements

    [
      {
        "id": 12345,
        "name": "Julian Doherty",
        "priceRange": {"min": 120, "max": 150}},
        "requirements": ["internet", "bath"],
      },
      ...
    ]
    
Accommodation
------------
`data/accommodation.json` contains a list of accommodation locations, price rate, attributes, and total/available capacity

    [
      {
        "id": 4321,
        "name": "Hotel Awesome",
        "price": 130,
        "attributes": [
          "close to transport", "internet", "bath"
        ],
        "capacity": {"total": 25, "free": 12}
      }
    ]

Requirements
------------
Write an application(s) that reads in both accommodation and traveller data files, and process as follows

  * for each traveller in the order they appear in the input file
    1. find the one accommodation location that:
      * is the cheapest within the traveller's price range
      * has **all** the traveller's requirements in the accommodation's attributes
      * has free capacity
    2. add the traveller to the list of bookings for the accommodation
    3. adjust the free capacity for the accommodation
  * iterate to next traveller

Once the data is loaded and processed, query the processed data in the following ways
  
  * list the travellers booked at an accommodation by accommodation id
  * show the accommodation (or none) where a traveller is booked by traveller id
  * given a price and requirements, find the best accommodation match name and price (or none) for a new traveller

Installation
------------
  - Run "bundle install" (assuming you have already Bundler gem).
  - It was developed with Ruby 1.9.3 but should work with any 1.9.x
  - Run "rspec spec" from root dir to run all tests.

How to use
----------
This code is to be driven via a command line application. e.g:
        
    $~> bin/lpbooker accommodation ./data/travellers.json ./data/accomodation.json 101
    Accommodation: Hotel Awesome
    ---
    Julian Doherty
    Homer Simpson
    
    
    $~> bin/lpbooker traveller ./data/travellers.json ./data/accomodation.json 100
    Traveller: Julian Doherty
    Booked at: Hotel Awesome
    
    
    $~> bin/lpbooker search ./data/travellers.json ./data/accomodation.json 120 150 internet bath, "close to transport"
    Hotel Awesome, 130


Implementation Notes
--------------------
  - Using indices for Accommodation matching.
  - Code needs clean up and some refactoring.
  - Test fixtures need work. Would be much better if I've used an object factory.

   
