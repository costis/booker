require 'spec_helper'

module Booker
  describe Accommodation do
    before(:each) do
      @accommodation = Accommodation.new
      @accommodation.price = 100
      @accommodation.attributes = ["attr1", "attr2", "attr3"]
      @accommodation.capacity = {"total" => 20, "free" => 10}
    end

    describe ".set_data" do
      before(:each) do
        accommodations_filepath = File.expand_path("../../fixtures/accommodation.json", __FILE__)
        json_file = File.read accommodations_filepath
        @accommodations = JSON.parse(json_file, {:object_class => Booker::Accommodation})
      end

      it "should set the collection" do
        Accommodation.set_data @accommodations
        Accommodation.all.should_not be_empty
      end

      context "Attributes Index" do
        it "should populate the Attributes Index" do
          Accommodation.set_data @accommodations
          Accommodation.attributes_index.should_not be_empty
        end

        it "Attributes Index should have the correct keys" do
          Accommodation.set_data @accommodations
          Accommodation.attributes_index.keys.should eq ["req 1", "req 2", "req 3", "req 6", "req 7"]
        end

        it "Attributes Index keys should have correct values" do
          Accommodation.set_data @accommodations
          Accommodation.attributes_index["req 1"].should eq [1, 2, 3, 4, 5]
          Accommodation.attributes_index["req 2"].should eq [1]
          Accommodation.attributes_index["req 3"].should eq [1, 2, 3, 4, 5]
          Accommodation.attributes_index["req 6"].should eq [3, 4, 5]
          Accommodation.attributes_index["req 7"].should eq [3, 5]
        end
      end

      context "Price Index" do
        it "should populate the Price Index" do
          Accommodation.set_data @accommodations
          Accommodation.price_index.should_not be_empty
        end
     end

      context "Availability Index" do
        it "should populate the Availability Index" do
          Accommodation.set_data @accommodations
          Accommodation.availability_index[:free].should_not be_empty
        end

        it "should have correct values" do
          Accommodation.set_data @accommodations
          Accommodation.availability_index[:free].should eq [1, 2, 3, 4, 5]
        end
      end
    end

    describe ".find_by_id" do
      it "should retrieve the correct accommodation" do
        res = Accommodation.find_by_id(1)
        res.should_not be_nil
        res.name.should eq "Accommodation 1"
      end
    end

    describe ".find_with_criteria" do
      context "when criteria matches" do
        before(:each) do
          accommodations_filepath = File.expand_path("../../fixtures/accommodation.json", __FILE__)
          json_file = File.read accommodations_filepath
          @accommodations = JSON.parse(json_file, {:object_class => Booker::Accommodation})
          Accommodation.set_data @accommodations
        end

        it "should find an Accommodation with given criteria", :wip => false do
          found = Accommodation.find_with_criteria(Range.new(100, 200), ["req 1", "req 3"])
          found.id.should eq 4
        end

        it "should find accommodations with all criteria matching #1", :wip => false do
          found = Accommodation.find_with_criteria(Range.new(100, 200), ["req 1", "req 3", "req 2"])
          found.id.should eq 1
        end

        it "should find accommodations with all criteria matching #2" do
          found = Accommodation.find_with_criteria(Range.new(100, 200), ["req 1", "req 3", "req 7", "req 6"])
          found.id.should eq 3
        end

        it "should find the cheapest accommodation if multiple accommodations match" do
          found = Accommodation.find_with_criteria(Range.new(100, 200), ["req 1", "req 3"])
          found.id.should eq 4
        end

        it "should return nil if no accommodations found", :wip => true do
          found = Accommodation.find_with_criteria({:min_price => 100, :max_price => 200}, ["req 1", "req 8"])
          found.should be_nil
        end
      end
    end

    describe ".find_for_customer" do
      it "should find an accommodation for customer" do
        accommodations_filepath = File.expand_path("../../fixtures/accommodation.json", __FILE__)
        json_file = File.read accommodations_filepath
        accommodations = JSON.parse(json_file, {:object_class => Booker::Accommodation})
        Accommodation.set_data accommodations

        customers_filepath = File.expand_path("../../fixtures/travellers.json", __FILE__)
        json_file = File.read customers_filepath
        customers = JSON.parse(json_file, {:object_class => Booker::Customer})
        customer = customers.find { |c| c.id == 1 }

        found = Accommodation.find_for_customer(customer)
        found.should_not be_nil
      end
    end

    describe "#matches_price" do
      it "should match when price is within given range" do
        res = @accommodation.matches_price(Range.new(90, 120))
        res.should be_true
      end

      it "should not match when price is outside of given range" do
        @accommodation.price = 400
        res = @accommodation.matches_price(Range.new(90, 120))
        res.should_not be_true
      end
    end

    describe "#matches_requirements" do
      it "should match if it has all given requirements" do
        res = @accommodation.matches_requirements ["attr1", "attr2", "attr3"]
        res.should be_true
      end

      it "should match if it has all given requirements in any order" do
        res = @accommodation.matches_requirements ["attr3", "attr2", "attr1"]
        res.should be_true
      end

      it "should not match if it doesn't have all given requirements" do
        res = @accommodation.matches_requirements ["attr3", "attr2", "attr6"]
        res.should_not be_true
      end
    end

    describe "#has_availability" do
      it "should match if it has free rooms" do
        res = @accommodation.has_availability
        res.should be_true
      end

      it "should not match if there are no free rooms" do
        @accommodation.capacity = {"total" => 20, "free" => 0}
        res = @accommodation.has_availability
        res.should_not be_true
      end
    end

    describe "#reduce_capacity" do
      it "should reduce free capacity by 1" do
        @accommodation.reduce_free_capacity
        @accommodation.free_capacity.should eq 9
      end
    end

  end
end
