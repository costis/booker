module Booker
  module HashExtension

    def method_missing(method, *params)
      method_string = method.to_s
      if method_string =~  /=$/
        self[method_string[0..-2]] = params.first
      else
        return self[method_string] if (key? method_string or key? method_string.to_sym)
        #self.each { |k,v| return v if k.to_s.to_sym == name }
        super.method_missing method_string
      end
    end

  end
end
