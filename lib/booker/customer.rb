require 'booker/hash_extension'

module Booker
  class Customer < Hash
    include Booker::HashExtension

    @all = []

    class << self
      attr_reader :all
    end

    def self.set_data(data)
      @all = data
    end

    def self.find_by_id(customer_id)
      @all.find { |customer| customer.id == customer_id}
    end

  end
end
