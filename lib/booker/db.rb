require 'json'
require 'booker/customer'

module Booker
  class DB

    def initialize(customers_file, accommodations_file)
      f1 = File.read customers_file
      f2 = File.read accommodations_file
      Customer.set_data (JSON.parse f1, { :object_class => Booker::Customer })
      Accommodation.set_data (JSON.parse f2, { :object_class => Booker::Accommodation })
    end

    def process_bookings_for_customer(customer)
      accommodation = Accommodation.find_with_criteria(Range.new(customer.priceRange["min"], customer.priceRange["max"]), customer.requirements)
      Booking.create_booking(customer, accommodation) if accommodation
    end

    def process_all_customers
      Customer.all.each do |customer|
        process_bookings_for_customer(customer)
      end
    end

    def display_data_for_accommodation accommodation_id
      process_all_customers
      bookings = Booking.find_by_accommodation_id accommodation_id
      if !bookings.empty?
        puts "Accommodation: #{bookings.first.accommodation.name}"
        puts "-------------"
        bookings.each do |b|
          puts b.customer.name
        end
      else
        puts "No bookings found for accommodation: #{accommodation_id}"
      end
    end

    def display_data_for_customer customer_id
      process_all_customers
      bookings = Booking.find_by_customer_id customer_id
      if !bookings.empty?
        puts "Customer: #{bookings.first.customer.name}"
        puts "-------------"
        bookings.each do |b|
          puts b.accommodation.name
        end
      else
        puts "No bookings found for customer: #{customer_id}"
      end
    end

    def display_data_for_search price_min, price_max, prefs
      accommodation = Accommodation.find_with_criteria price_min..price_max, prefs 
      if accommodation
        puts "#{accommodation.name}, #{accommodation.price}"
      else
        puts "No accommodations found"
      end
    end

  end
end
