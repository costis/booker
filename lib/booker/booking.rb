module Booker

  class Booking
    @all = []
    attr_reader :customer, :accommodation

    class << self
      attr_reader :all
    end

    def initialize(customer, accommodation)
      @customer = customer
      @accommodation = accommodation
    end

    def self.create_booking(customer, accommodation)
      booking = Booking.new(customer, accommodation)
      @all << booking
      accommodation.reduce_free_capacity
      return booking
    end

    def self.find_by_accommodation_id(accommodation_id)
      @all.select { |item| item.accommodation.id == accommodation_id}
    end

    def self.find_by_customer_id(customer_id)
      @all.select { |item| item.customer.id == customer_id}
    end

  end
end
