require 'booker/hash_extension'
require 'benchmark'

module Booker
  class Accommodation < Hash
    include Booker::HashExtension

    @all = []
    @attributes_index   = Hash.new
    @price_index        = Hash.new
    @availability_index = {:free => []}

    class << self
      attr_accessor :all, :attributes_index, :price_index, :availability_index
    end

    def self.set_data(data)
      @all = data

      @all.each do | ac |
        # populate Attribute Index
        ac.attributes.each do |attr|
          if self.attributes_index.has_key? attr
            self.attributes_index[attr] << ac.id
            self.attributes_index[attr].uniq!
          else
            self.attributes_index[attr] = [ac.id]
          end
        end

        # populate Price Index
        if self.price_index.has_key? ac.price
          self.price_index[ac.price] << ac.id
        else
          self.price_index[ac.price] = [ac.id]
        end
        self.price_index[ac.price].uniq!

        # populate Availability Index
        if ac.capacity["free"] > 0
          self.availability_index[:free] << ac.id
        end
        self.availability_index[:free].uniq!
      end
    end

    def self.rebuild_availability_index! id_arr
        self.availability_index[:free] = self.availability_index[:free] - id_arr
    end

    def self.find_by_id(accommodation_id)
      @all.find {|item| item.id == accommodation_id}
    end

    def self.find_with_criteria(price_range, requirements)
      found = []
      requirements.each_with_index do |attr, index|
        if self.attributes_index.key? attr
          if index == 0
            found = self.attributes_index[attr]
          else
            found = found & self.attributes_index[attr]
            break if found.empty?
          end
        else
          found.clear
          break
        end
      end

      # return if no match on requirements
      return nil if(!requirements.empty? and found.empty?)

      # try a match on availability index
      if !found.empty?
        found &= self.availability_index[:free]
      else
        found = self.availability_index[:free].dup
      end
      # return if we couldn't match on availability
      return nil if found.empty?

      # search for matches on price
      matched = false
      price_range.each do |price|
        if requirements.empty? and self.price_index.key? price
          found << self.price_index[price].first
          matched = true
          break
        else
          if self.price_index.key? price
            check = found & self.price_index[price]
            if !check.empty?
              found = check
              matched = true
              break
            end
          end
        end
      end
      # return if we couldn't match on price
      return nil if !matched

      return self.find_by_id found.first
    end

    def self.find_for_customer(customer)
      price_range = Range.new(customer.priceRange["min"], customer.priceRange["max"])
      self.find_with_criteria(price_range, customer.requirements)
    end

    def free_capacity
      capacity["free"].to_i
    end

    def free_capacity=(new_capacity)
      capacity["free"] = new_capacity
    end

    def has_availability
      free_capacity > 0
    end

    def reduce_free_capacity
      self.free_capacity -= 1
      self.class.rebuild_availability_index! [self.id] unless has_availability
    end

    def matches_requirements(requirements)
        requirements - attributes == []
    end

    def matches_price(price_range)
        price_range === price
    end

  end
end
