require 'spec_helper'

module Booker
  describe DB do

    describe "#initialize" do
      before(:each) do
        @travellers_filepath = File.expand_path("../../fixtures/travellers.json", __FILE__)
        @accommodations_filepath = File.expand_path("../../fixtures/accommodation.json", __FILE__)
        @db = Booker::DB.new(@travellers_filepath, @accommodations_filepath)
      end

      it "should load Customer data from json files" do
        Customer.stub(:set_data)
        Customer.should_receive(:set_data)
        Booker::DB.new(@travellers_filepath, @accommodations_filepath)
      end

      it "should load Accommodation data customer data from json files" do
        Accommodation.stub(:set_data)
        Accommodation.should_receive(:set_data)
        Booker::DB.new(@travellers_filepath, @accommodations_filepath)
      end

      it "should raise exception if file path is incorrect" do
        lambda { Booker::DB.new('./file-does-not-exist.json', './another-nonexistant-file.json') }.should raise_error(Errno::ENOENT)
      end
    end

    describe "#process_bookings_for_customer" do
      before(:each) do
        travellers_filepath = File.expand_path("../../fixtures/travellers.json", __FILE__)
        accommodations_filepath = File.expand_path("../../fixtures/accommodation.json", __FILE__)
        @db = Booker::DB.new(travellers_filepath, accommodations_filepath)

        customers_file = File.read travellers_filepath
        accommodations_file = File.read accommodations_filepath
        @customers = JSON.parse(customers_file, {:object_class => Booker::Customer})
        @accommodations = JSON.parse(accommodations_file, {:object_class => Booker::Accommodation})

        @customer = Customer.find_by_id 1
        @accommodation = Accommodation.find_by_id 1
      end

      it "should try to find an Accommodation for given Customer" do
        Accommodation.stub(:find_with_criteria).with(:any)
        price_range = Range.new(@customer.priceRange["min"], @customer.priceRange["max"])
        Accommodation.should_receive(:find_with_criteria).with(price_range, @customer.requirements).and_return(@accommodation)
        @db.process_bookings_for_customer(@customer)
      end

      it "should create a booking for Customer if a matching Accommodation is found" do
        accommodations = mock(Accommodation)
        price_range = Range.new(@customer.priceRange["min"], @customer.priceRange["max"])
        accommodations.stub(:find_with_criteria).with(price_range, @customer.requirements).and_return(@accommodation)

        Booking.should_receive(:create_booking).with(@customer, @accommodation)
        @db.process_bookings_for_customer(@customer)
      end

      it "should not create a booking if no matching accommodation is found" do
        Accommodation.stub(:find_with_criteria).and_return nil

        Booking.stub(:create_booking)
        Booking.should_not_receive(:create_booking)
        @db.process_bookings_for_customer(@customer)
      end
    end

    describe "#process_all_customers" do
      before(:each) do
        Booking.all.clear
      end

      it "should create bookings" do
        travellers_filepath = File.expand_path("../../fixtures/travellers.json", __FILE__)
        accommodations_filepath = File.expand_path("../../fixtures/accommodation.json", __FILE__)
        @db = Booker::DB.new(travellers_filepath, accommodations_filepath)
        @db.process_all_customers
        Booking.all.should_not be_empty
      end

      context "availability exhaustion scenario" do
        before(:each) do
          travellers_filepath = File.expand_path("../../fixtures/availability-exhaustion-scenario/travellers.json", __FILE__)
          accommodations_filepath = File.expand_path("../../fixtures/availability-exhaustion-scenario/accommodation.json", __FILE__)
          @db = Booker::DB.new(travellers_filepath, accommodations_filepath)
          @db.process_all_customers
        end

        it "should create 4 Bookings" do
          Booking.all.size.should eq 4
        end

        it "should create Bookings only for the first 4 Customers, in order" do
          Booking.find_by_accommodation_id(1).collect { |booking| booking.customer.id }.should eq [1, 2, 3, 4]
        end

        it "should create Bookings only for Accommodation with id 1" do
          Booking.find_by_accommodation_id(1).collect { |booking| booking.accommodation.id }.uniq.should eq [1]
        end

        it "should not create a Booking for the 5th Customer (availability runs out)" do
          Booking.find_by_customer_id(5).should be_empty
        end
      end


      it "generic test", :slow => true, :if => false do
        travellers_filepath = File.expand_path("../../../data/travellers.json", __FILE__)
        accommodations_filepath = File.expand_path("../../../data/accommodation.json", __FILE__)
        @db = Booker::DB.new(travellers_filepath, accommodations_filepath)
        @db.process_all_customers
        Booking.all.should_not be_empty
        puts "bookings size: #{Booking.all.size}"
        puts "customers size: #{Customer.all.size}"
        puts "accommodations size: #{Accommodation.all.size}"

        arr = []
        Booking.all.each do |b|
          arr << "#{b.customer.id}"
          #arr << "(#{b.customer.name})#{b.customer.id}_#{b.accommodation.id}(#{b.accommodation.name})"
        end
        str = arr.join(",")
        File.open('bookings_perf.txt', 'w') { |f| f.write(str)}
      end

    end


  end

end
